---
title: "#60 Linux Express"
date: 2019-02-06
author: juan
category: [linuxexpress]
featimg: 2019/60linuxexpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/60linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2019/60linuxexpress.png)  
<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/60linuxexpress.ogg" type="audio/ogg">
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/60linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ Episodio [#70 Alternativas Libres a Google](https://avpodcast.net/podcastlinux/alternativasgoogle/)
+ Próximo episodio: Linux Connexion con [Victorhck](https://victorhckinthefreeworld.com/)
+ Nuevos productos [Pine64](https://pine64.org)
+ Nuevo proyecto en mente: NAS Doméstico
+ [DNI Electrónico y GNU/Linux](https://twitter.com/podcastlinux/status/1091639622105550848)
+ Blogs en Org-mode Emacs
+ Territorio f-Droid: [Syncthing](https://f-droid.org/es/packages/com.nutomic.syncthingandroid/)
+ Nos vemos en [FLISol 19 Tenerife](https://flisol.info/FLISOL2019/Espana/Tenerife)



Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-2322821).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  
