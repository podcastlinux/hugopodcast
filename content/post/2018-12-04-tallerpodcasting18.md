---
title: "Taller Podcasting Libre"
date: 2018-12-04
author: juan
category: [taller, podcasting]
featimg: 2018/tallerpodcasting.png
podcast:
  audio: 
  video:
tags: [taller, podcasting]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2018/tallerpodcasting.png)  
El 7 de diciembre realizo un [taller de Podcasting libre](https://linuxcenter.es/aprende/proximos-eventos/39-juan-febles-de-podcast-linux-viene-a-linux-center?date=2018-12-07-18-30) en [Linux Center](https://linuxcenter.es/). Aquí te dejo toda la documentación de éste.

**Enlaces de Interés:**
+ [Curso Podcasting Podcast Linux](https://podcastlinux.com/cursopodcasting/)
+ [Curso Audacity 9 Decibelios](https://9decibelios.es/cursos/curso-basico-de-audacity/)
+ [TheAudacityToPodcast.com](https://theaudacitytopodcast.com/chriss-dynamic-compressor-plugin-for-audacity/)
+ [Wiki Audacity](https://wiki.audacityteam.org/wiki/Audacity_Wiki_Home_Page)
+ [Repositorio Gitlab](https://gitlab.com/podcastlinux/tallerpodcasting/)

**Presentación:**  
+ [Archivo .odp](https://gitlab.com/podcastlinux/tallerpodcasting/raw/master/charla/TallerPodcastingLibre.odp)
+ [Archivo pdf](https://gitlab.com/podcastlinux/tallerpodcasting/raw/master/charla/TallerPodcastingLibre.pdf)

**Audacity**
+ [Preset Ecualización Voz](https://gitlab.com/podcastlinux/tallerpodcasting/raw/master/audacity/EQVoz.xml)
+ [Filtro Compresión](https://gitlab.com/podcastlinux/tallerpodcasting/raw/master/audacity/compress.ny)

**Vídeo:**  
<iframe width="560" height="315" src="https://www.youtube.com/embed/R7KBSSVLMeU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Recuerda que puedes **contactar** conmigo de las siguientes maneras:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.gitlab.io/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  
